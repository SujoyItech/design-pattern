/**
 * Decorator Design Pattern
 * 
 * The Decorator Design Pattern is a structural pattern that allows behavior to be added to an individual object,
 * either statically or dynamically, without affecting the behavior of other objects from the same class.
 * It is a flexible alternative to subclassing for extending functionality.
 * 
 * Component: Declares the interface for objects that can have responsibilities added dynamically. 
 * In TypeScript, this is often an interface or an abstract class.
 * 
 * ConcreteComponent: Defines an object to which additional responsibilities can be attached. Implements the Component interface.
 * 
 * Decorator: Maintains a reference to a Component object and defines an interface that conforms to the Component's interface. 
 * Can add responsibilities before and/or after forwarding requests to the component.
 * 
 * ConcreteDecorator: Adds responsibilities to the component. Extends the behavior of the Component
 * 
 * Use Case: Consider a scenario where you have a simple coffee order system.
 * You have a basic coffee class, and you want to add various optional condiments
 * (decorators) like milk, sugar, or whipped cream.
 */

// Component
interface Coffee {
    cost(): number;
    description(): string;
}

// ConcreteComponent
class SimpleCoffee implements Coffee {
    cost(): number {
        return 5;
    }
    description(): string {
        return "Simple Coffee";
    }
}

// Decorator 
abstract class CoffeeDecorator implements Coffee {
    protected coffee: Coffee;

    constructor(coffee: Coffee) {
        this.coffee = coffee;
    }

    abstract cost(): number;
    abstract description(): string;
}

// Concrete Decorator
class MilkDecorator extends CoffeeDecorator {
    cost(): number {
        return this.coffee.cost() + 2;
    }
    description(): string {
        return this.coffee.description() + ", Milk";
    }
}

// Concrete Decorator
class SugarDecorator extends CoffeeDecorator {
    cost(): number {
        return this.coffee.cost() + 1;
    }
    description(): string {
        return this.coffee.description() + ", Sugar";
    }
}

// Usage
const myCoffee: Coffee = new SimpleCoffee();
console.log(myCoffee.description()); // Output: Simple Coffee

const milkCoffee: Coffee = new MilkDecorator(myCoffee);
console.log(milkCoffee.description()); // Output: Simple Coffee, Milk
console.log(milkCoffee.cost()); // Output: 7

const sweetCoffee: Coffee = new SugarDecorator(milkCoffee);
console.log(sweetCoffee.description()); // Output: Simple Coffee, Milk, Sugar
console.log(sweetCoffee.cost()); // Output: 8

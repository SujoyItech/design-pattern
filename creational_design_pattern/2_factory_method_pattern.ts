/**
 * Factory Method Design Pattern
 * 
 * Real World Example: Consider the case of a hiring manager. It is impossible for one person
 * to interview for each of the positions. Based on the job opening, she has to decide and delegate
 * the interview steps to different people
 * 
 * Plain Words: It provides a way to delegate the instantiation logic to child classes.
 * 
 * Useage: Useful when there is some generic processing in a class but the required 
 * sub-class is dynamically decided at runtime.
 * Or putting it in other words, when the client doesn't know what exact sub-class it might need.
 */

// Product interface
class Animal {
    private readonly name: string;
    constructor(name: string) {
        this.name = name;
    }

    makeSound() {
        throw new Error("This method should be overridden by subclasses");
    }
}

class Dog extends Animal {
    makeSound() {
        return "Dog!";
    }
}

class Cat extends Animal {
    makeSound() {
        return "Cat!";
    }
}

class AnimalFactory {
    createAnimal(name: string) {
        throw new Error("This method should be overridden by subclasses");
    }
}

class DogFactory extends AnimalFactory {
    createAnimal(name: string) {
        return new Dog(name);
    }
}

class CatFactory extends AnimalFactory {
    createAnimal(name: string) {
        return new Cat(name);
    }
}

const dogFactory = new DogFactory();
const catFactory = new CatFactory();

const myDog = dogFactory.createAnimal("Tom");
const myCat = catFactory.createAnimal("Cattu");

console.log(myDog.makeSound());
console.log(myCat.makeSound());

/**
 * Builder Design Pattern
 * 
 * The Builder design pattern is a creational design pattern that is used to 
 * construct a complex object step by step. It allows you to produce different types
 * and representations of an object using the same construction process.
 * This pattern is particularly useful when an object has a large number of parameters,
 * and you want to provide a clean way to construct it.
 */

// Product: The object we want to build
class House {
    private foundation: string = "";
    private walls: string = "";
    private roof: string = "";

    setFoundation(foundation: string){
        this.foundation = foundation;
    }

    setWalls(walls: string) {
        this.walls = walls;
    }

    setRoof(roof: string) {
        this.roof = roof;
    }
    
    getDetails(): string {
        return `House with ${this.foundation}, ${this.walls} walls, ${this.roof} roofs`;
    }
}

// Builder: Interface for building the product
interface HouseBuilder {
    buildFoundation(): void;
    buildWalls(): void;
    buildRoof(): void;
    getHouse(): House;
}

// Concrete Builder: Implements the builder interface to construct a specific product
class SimpleHouseBuilder implements HouseBuilder {
    private house: House = new House();

    buildFoundation(): void {
        this.house.setFoundation("simple");
    }

    buildWalls(): void {
        this.house.setWalls("basic");
    }

    buildRoof(): void {
        this.house.setRoof("standard");
    }

    getHouse(): House {
        return this.house;
    }
}

// Director: Manages the construction process using a builder

class HouseDirector {
    private builder: HouseBuilder;

    constructor(builder: HouseBuilder) {
        this.builder = builder;
    }

    constructHouse() {
        this.builder.buildFoundation();
        this.builder.buildWalls();
        this.builder.buildRoof();
    }

    getConstructedHouse() {
        return this.builder.getHouse();
    }
}

// Client code

const simpleHouseBuilder = new SimpleHouseBuilder();
const director = new HouseDirector(simpleHouseBuilder);

director.constructHouse();
const simpleHouse = director.getConstructedHouse();

console.log(simpleHouse.getDetails());

/**
 * 
 * House: is the product we want to build.
 * HouseBuilder: is the interface that defines the construction process.
 * SimpleHouseBuilder: is a concrete builder that implements the HouseBuilder interface to build a specific type of house.
 * HouseDirector: is responsible for managing the construction process using a builder.
 * 
 * The client code creates a SimpleHouseBuilder, passes it to a HouseDirector,
 * and then uses the director to construct a house. Finally, 
 * the client retrieves the constructed house and prints its details.
 * 
 */
/**
 * Singleton Design Pattern
 * 
 * The Singleton design pattern ensures that a class has only one instance 
 * and provides a global point of access to that instance. 
 * This is useful when exactly one object is needed to coordinate actions across the system.
 * 
 * In TypeScript, you can implement the Singleton pattern in various ways, 
 * but I'll demonstrate a simple example using a class with a private constructor and a static method to get the instance.
 */

class Singleton {
    private static instance: Singleton | null = null; // Static variable to hold instance
    private data: string; // Example property of the singleton

    // Private constructor to prevent direct instantiation
    private constructor() {
        this.data = "Singleton instance created.";
    }

    static getInstance(): Singleton {
        if(!Singleton.instance) {
            Singleton.instance = new Singleton();
        }
        return Singleton.instance;
    }

    // Example method
    getData(): string {
        return this.data;
    }
}

// Client code
const singletonInstance1 = Singleton.getInstance();
console.log(singletonInstance1.getData()); // Output: Singleton instance created

const singletonInstance2 = Singleton.getInstance();
console.log(singletonInstance2.getData()); // Output: Singleton instance created

// Check if both instances are the same
console.log(singletonInstance1 == singletonInstance2);
/**
 * Bridge Design Pattern
 *
 * The Bridge design pattern is a structural pattern that separates abstraction from implementation.
 * It allows the two to vary independently. This pattern is particularly useful
 * when you want to avoid a permanent binding between an abstraction and its implementation,
 * allowing them to evolve independently. The Bridge pattern is achieved by creating
 * a bridge interface that contains a reference to an implementation interface.
 *
 * Let's look at a TypeScript example to understand this pattern better.
 *
 */

// Abstraction: Shape
interface Shape {
  draw(): void;
}

// Implementor: DrawingAPI
interface DrawingAPI {
  drawCircle(x: number, y: number, redius: number): void;
  drawSquare(x: number, y: number, side: number): void;
}

// Concrete Implementor: DrawingAPI1
class DrawingAPI1 implements DrawingAPI {
  drawCircle(x: number, y: number, radius: number): void {
    console.log(`API1 - Drawing circle at (${x},${y}) with radius ${radius}`);
  }
  drawSquare(x: number, y: number, side: number): void {
    console.log(`API1 - Drawing square at (${x},${y}) with side ${side}`);
  }
}

// Concrete Implementor: DrawingAPI2
class DrawingAPI2 implements DrawingAPI {
  drawCircle(x: number, y: number, radius: number): void {
    console.log(`API2 - Drawing circle at (${x},${y}) with radius ${radius}`);
  }
  drawSquare(x: number, y: number, side: number): void {
    console.log(`API2 - Drawing square at (${x},${y}) with side ${side}`);
  }
}

// Refined Abstraction: Circle
class Circle implements Shape {
  constructor(
    private x: number,
    private y: number,
    private radius: number,
    private drawingAPI: DrawingAPI
  ) {}

  draw(): void {
    this.drawingAPI.drawCircle(this.x, this.y, this.radius);
  }
}

// Refined Abstraction: Square
class Square implements Shape {
  constructor(
    private x: number,
    private y: number,
    private side: number,
    private drawingAPI: DrawingAPI
  ) {}

  draw(): void {
    this.drawingAPI.drawSquare(this.x, this.y, this.side);
  }
}
// Client code
const api1 = new DrawingAPI1();
const api2 = new DrawingAPI2();

const circle1 = new Circle(1, 2, 3, api1);
const circle2 = new Circle(4, 5, 6, api2);

const square1 = new Square(7, 8, 9, api1);
const square2 = new Square(10, 11, 12, api2);

circle1.draw();
circle2.draw();
square1.draw();
square2.draw();

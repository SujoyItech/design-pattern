/**
 * Real world example: 
 * Extending our door example from Simple Factory. 
 * Based on your needs you might get a wooden door from a wooden door shop,
 * iron door from an iron shop or a PVC door from the relevant shop. 
 * Plus you might need a guy with different kind of specialities to fit the door,
 * for example a carpenter for wooden door, welder for iron door etc. 
 * As you can see there is a dependency between the doors now, wooden door needs carpenter, iron door needs a welder etc.
 * 
 * 
 * In plain words: A factory of factories a factory that groups the individual 
 * but related/dependent factories together without specifying their concrete classes.
 * 
 * Useage: When there are interrelated dependencies with not-that-simple creation logic involved
 */

class GlassDoor {
    getDescription() {
        console.log('I am a glass door')
    }
}

class IronDoor {
    getDescription() {
        console.log('I am a irone door')
    }
}

class Welder {
    getDescription() {
        console.log('I can only fit iron doors')
    }
}

class Carpenter {
    getDescription() {
        console.log('I can only fit glass doors')
    }
}

// Glass factory to return carpenter and glass door
class GlassDoorFactory {
    makeDoor(){
        return new GlassDoor()
    }

    makeFittingExpert() {
        return new Carpenter()
    }
}

// Iron door factory to get iron door and the relevant fitting expert
class IronDoorFactory {
    makeDoor(){
        return new IronDoor()
    }

    makeFittingExpert() {
        return new Welder()
    }
}

const glassFactory = new GlassDoorFactory()

let doorObj = glassFactory.makeDoor()
let expert = glassFactory.makeFittingExpert()

doorObj.getDescription()  // Output: I am a wooden door
expert.getDescription() // Output: I can only fit wooden doors

// Same for Iron Factory
const ironFactory = new IronDoorFactory()

doorObj = ironFactory.makeDoor()
expert = ironFactory.makeFittingExpert()

doorObj.getDescription()  // Output: I am an iron door
expert.getDescription() // Output: I can only fit iron doors
/**
 * Adaper Design Pattern
 * 
 * The Adapter design pattern allows the interface of an existing class to be used as another interface.
 * It is often used to make existing classes work with others without modifying their source code.
 * This pattern is useful when you have incompatible interfaces, and you want to make them work together.
 * 
 * Let's look at a TypeScript example using the Adapter pattern.
 */

// Suppose we have an existing OldSystem class with a method called requestData:
class OldSystem {
    requestData() {
        return "Data from the old system";
    }
}

// Now we have a new system that expects an interface with a method called fetchData
interface NewSystem {
    fetchData(): string;
}

// We want to adapt the Oldsystem to the Newsystem interface without modifying the
// existing Oldsystem class. Here is how you can do it using the Adapter pattern

class Adapter implements NewSystem {
    private oldSystem: OldSystem;

    constructor(oldSystem: OldSystem) {
        this.oldSystem = oldSystem;
    }

    fetchData(): string {
        return this.oldSystem.requestData();
    }
}

// Client code using the NewSystem interface
function clientCode(system: NewSystem): void {
    const data = system.fetchData();
    console.log(data);
}

// Using the adapter to make OldSystem work with the NewSystem interface
const oldSystem = new OldSystem();
const adaptedSystem = new Adapter(oldSystem);

// Calling the client code with the adapted system
clientCode(adaptedSystem);

/**
 * OldSystem: is the existing class with a method named requestData.
 * NewSystem: is the target interface that our client code expects.
 * Adapter: is the adapter class that implements the NewSystem interface but internally uses an instance of OldSystem.
 * The fetchData method in the Adapter class adapts the call to requestData.
 * 
 * The clientCode function is the client code that expects a NewSystem interface.
 * We can pass an instance of Adapter to this function to make the OldSystem work seamlessly.
 */
/**
 * Simple Factory Design Pattern
 * 
 * Real World Example:  Consider, you are building a house and you need doors. It would be a mess if every time 
 * you need a door, you put on your carpenter clothes and start making a door in your house. 
 * Instead you get it made from a factory.
 * 
 * Plain Words: Simple factory simply generates an instance for client without exposing 
 * any instantiation logic to the client
 * 
 * Useage: When creating an object is not just a few assignments and involves some logic,
   it makes sense to put it in a dedicated factory instead of repeating the same code everywhere
 */

// First of all we have a door interface and the implementation
class WoodenDoor {
    private readonly width: number;
    private readonly height: number;

    constructor(width: number, height: number) {
        this.width = width;
        this.height = height;
    }

    getWidth(): number {
        return this.width;
    }

    getHeight(): number {
        return this.height;
    }
}

// Then we have a door factory that makes the door and returns it
const DoorFactory = {
    makeDoor: (width: number, height: number) => new WoodenDoor(width, height)
}

// And then it can be used as
const door = DoorFactory.makeDoor(100, 200);
console.log('Width', door.getWidth());
console.log('Height', door.getHeight());


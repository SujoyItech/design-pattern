/**
 * Configuration Management
 *
 * When your application needs to access configuration settings from a single source,
 * a Singleton can be used to manage and provide access to the configuration data.
 */

class ConfigurationManager {
  private static instance: ConfigurationManager | null = null;
  private config: any;

  private constructor() {
    this.config = "" /* Load configuration from a file, database, etc. */;
  }

  static getInstance(): ConfigurationManager {
    if (!ConfigurationManager.instance) {
      ConfigurationManager.instance = new ConfigurationManager();
    }
    return ConfigurationManager.instance;
  }

  getConfig(): any {
    return this.config;
  }
}

const configManager = ConfigurationManager.getInstance();
const appConfig = configManager.getConfig();

/**
 * Logging
 * 
 * Singleton is often used in logging frameworks where you want a single point to manage log entries.
 * This ensures that logs are consistent and can be controlled from a central location.
 */

class Logger {
    private static instance: Logger | null = null;

    constructor() {
        // Initialize logger
    }

    static getInstance(): Logger {
        if(!Logger.instance) {
            Logger.instance = new Logger();
        }
        return Logger.instance;
    }

    log(message: string): void {
        // Log messages
    }
}


const logger = Logger.getInstance();
logger.log("Log entry");

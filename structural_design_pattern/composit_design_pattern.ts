/**
 * Composit Design Pattern
 * 
 * The Composite Design Pattern is a structural design pattern that 
 * lets you compose objects into tree-like structures to represent part-whole hierarchies.
 * This pattern allows clients to treat individual objects and compositions of objects uniformly
 * 
 * Compose objects into tree structures to represent part-whole hierarchies.
 * Clients can treat individual objects and compositions of objects uniformly.
 */

// Component
interface FileSystemComponent {
    display(): void;
}

// Leaf
class SingleFile implements FileSystemComponent {
    constructor(private name: string) {}
    display(): void {
        console.log(`File: ${this.name}`)
    }
}

// Composit
class Directory implements FileSystemComponent {
    private children: FileSystemComponent[] = [];

    constructor(private name: string) {};

    add(component: FileSystemComponent): void {
        this.children.push(component);
    }
    
    display(): void {
        console.log(`Directory: ${this.name}`);
        for (const child of this.children) {
            child.display();
        }
    }
}

// Usage
const file1 = new SingleFile("file1.txt");
const file2 = new SingleFile("file2.txt");

const directory1 = new Directory("Folder 1");
const directory2 = new Directory("Folder 2");

directory1.add(file1);
directory1.display();

directory2.add(file2);
directory2.display();

const root = new Directory("root");
root.add(directory1);
root.add(directory2);
root.display();

/**
 * Part-Whole Hierarchies: The Composite pattern is particularly useful when dealing with structures 
 * that can be represented as part-whole hierarchies, and you want to treat individual elements and compositions uniformly.
 * Tree Structures: Any tree-like structure where nodes can be either leaves or composites can benefit from the Composite pattern.
 * 
 * Benefits: Simplifies client code, allows for the creation of complex structures, and provides a uniform interface.
 * Drawbacks: May make the design overly general, and efficiency concerns may arise when dealing with a large number of components.
 */


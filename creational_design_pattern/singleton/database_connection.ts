/**
 * Database Connections
 * 
 * When you have a database connection that is expensive to create or requires a limited number of connections,
 * a Singleton can manage that connection and ensure that only one instance of the connection is used throughout the application.
 */

class DatabaseConnection {
    private static instance: DatabaseConnection | null = null;
    private constructor() {
        // Initialize database connection
    }

    static getInstance(): DatabaseConnection {
        if(!DatabaseConnection.instance) {
            DatabaseConnection.instance = new DatabaseConnection();
        }
        return DatabaseConnection.instance;
    }

    // Other database related methods
}

const dbConnection = DatabaseConnection.getInstance();